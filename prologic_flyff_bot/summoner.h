#ifndef SUMMONER_H
#define SUMMONER_H

unsigned long get_module(unsigned long pid, const char *module_name, unsigned long *size = 0);

#endif