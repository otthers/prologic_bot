#include "summoner.h"

#include <Windows.h>
#include <stdio.h>

int main() {
	// finding flyff window
	HWND hwnd = FindWindowA(0, "Prologic Flyff");
	if (hwnd) {
		unsigned long pid;
		GetWindowThreadProcessId(hwnd, &pid);

		HANDLE handle = OpenProcess(PROCESS_VM_OPERATION | PROCESS_VM_READ | PROCESS_VM_WRITE, 0, pid);
		if (handle) {
			unsigned long base_addr = 0;

			base_addr = get_module(pid, "Neuz.exe");

			// bypassing ani cheat engine
			if (WriteProcessMemory(handle, (LPVOID)(base_addr + 0x3F3286), "\xEB", 1, 0) == TRUE) {
				printf("bypassed anti cheat\n");
			}
		} else printf("can't open process\n");
	} else printf("can't find window\n");
	system("pause");
	return 0;
} 